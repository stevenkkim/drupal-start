; Makefile for "Start" distribution profile
; Command: drush make build-start.make <target directory>

core = 7.x
api = 2

; Drupal .......................................................................
projects[] = drupal

projects[start][type] = profile
projects[start][download][url] = https://github.com/stevenkkim/drupal-start.git























; Administration ...............................................................
projects[] = admin_menu

projects[] = module_filter
projects[] = rename_admin_paths

; Development ..................................................................
projects[] = devel
; projects[] = site_audit



; Tools ........................................................................
projects[] = ctools
projects[] = views

projects[] = token
projects[] = features
projects[] = libraries
projects[] = entity
projects[] = job_scheduler


; URLs .........................................................................
projects[] = pathauto
projects[] = redirect
projects[] = globalredirect
; projects[] = path_redirect



; JavaScript ...................................................................
; projects[] = jquery_update



; Input filters ................................................................
projects[] = markdown
projects[customfilter][version] = 1.x-dev
projects[] = token_filter
projects[] = typogrify
projects[] = pathologic






; Content types ................................................................
projects[] = fences
projects[] = field_formatter_settings
projects[] = field_delimiter
projects[] = filefield_paths
projects[] = field_group
projects[file_aliases][version] = 1.x-dev
projects[] = transliteration
projects[] = imagefield_tokens
projects[] = multiupload_filefield_widget
projects[] = multiupload_imagefield_widget
projects[] = smart_trim

; Blocks .......................................................................
projects[] = boxes
projects[] = block_class







; Menus ........................................................................
projects[] = menu_block
projects[] = menu_html
projects[] = taxonomy_menu
; projects[] = taxonomy_menu_block





projects[] = file_entity









; Login / account creation .....................................................
projects[] = email_registration
projects[] = user_registrationpassword
projects[] = login_destination
projects[] = r4032login






; Analytics ....................................................................
projects[] = google_analytics
; projects[] = piwik


; Mail .........................................................................
projects[] = mailsystem
projects[] = mimemail
projects[] = mailchimp
libraries[mailchimp][download][type] = "get"
libraries[mailchimp][download][url] = "http://apidocs.mailchimp.com/api/downloads/mailchimp-api-class.zip"
libraries[mailchimp][directory_name] = "mailchimp"
libraries[mailchimp][destination] = "libraries"
projects[] = mandrill
libraries[mandrill][download][type] = "get"
libraries[mandrill][download][url] = "https://bitbucket.org/mailchimp/mandrill-api-php/get/1.0.52.tar.gz"
libraries[mandrill][directory_name] = "mandrill"
libraries[mandrill][destination] = "libraries"











; Themes .......................................................................
projects[] = zen

projects[zensub][type] = "theme"
projects[zensub][download][url] = "https://github.com/stevenkkim/drupal-zensub.git"

projects[sevensub][type] = "theme"
projects[sevensub][download][url] = "https://github.com/stevenkkim/drupal-sevensub.git"









projects[] = fontyourface
; projects[] = font_awesome
; projects[] = icon
; projects[] = fontello
; projects[] = icomoon

















projects[addthis][version] = 4.x-dev








projects[] = backup_migrate



projects[feeds][version] = 2.x-dev



projects[feeds_tamper][version] = 1.x-dev

