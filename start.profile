<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function start_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}


/*
function start_install_finished(&$install_state) {
  drupal_set_title(st('@drupal installation complete', array('@drupal' => drupal_install_profile_distribution_name())), PASS_THROUGH);
  $messages = drupal_set_message();
  $output = '<p>' . st('Congratulations, you installed @drupal!', array('@drupal' => drupal_install_profile_distribution_name())) . '</p>';
  $output .= '<p>' . (isset($messages['error']) ? st('Review the messages above before visiting <a href="@url">your new site</a>.', array('@url' => url(''))) : st('<a href="@url">Visit your new site</a>.', array('@url' => url('')))) . '</p>';

  // Flush all caches to ensure that any full bootstraps during the installer
  // do not leave stale cached data, and that any content types or other items
  // registered by the installation profile are registered correctly.
  drupal_flush_all_caches();

  // Add the menu link
  $link = array('menu_name' => 'menu-administrator', 'link_path' => 'import', 'link_title' => 'Import', 'weight' => 1);
  $link = array('menu_name' => 'menu-administrator', 'link_path' => 'node/add', 'link_title' => 'Import', 'weight' => 1);
  menu_link_save($link);
  menu_rebuild();

  // Remember the profile which was used.
  variable_set('install_profile', drupal_get_profile());

  // Installation profiles are always loaded last
  db_update('system')->fields(array('weight' => 1000))->condition('type', 'module')->condition('name', drupal_get_profile())->execute();

  // Cache a fully-built schema.
  drupal_get_schema(NULL, TRUE);

  // Run cron to populate update status tables (if available) so that users
  // will be warned if they've installed an out of date Drupal version.
  // Will also trigger indexing of profile-supplied content or feeds.
  drupal_cron_run();

  return $output;
}
 */


/**
 * Implements hook_install_tasks().
 */
function start_install_tasks($install_state) {
  $tasks['add_import_link'] = array(
    'display_name' => st('Adds import link to menu-administrator'),
    'display' => TRUE,
    'type' => 'normal',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    'function' => 'callback_add_import_link',
  );

  return $tasks;
}

function callback_add_import_link() {
  // Add the menu link
  $link = array('menu_name' => 'menu-administrator', 'link_path' => 'import', 'link_title' => 'Import', 'weight' => 5);
  menu_link_save($link);
  $link = array('menu_name' => 'menu-administrator', 'link_path' => 'node/add', 'link_title' => 'Node Add', 'weight' => 6);
  menu_link_save($link);
  menu_rebuild();
}


